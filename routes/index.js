var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/imgs/:filename',function(req,res,next){
	console.log(req.params.filename);
	res.sendFile('images/'+req.params.filename);
});

module.exports = router;
