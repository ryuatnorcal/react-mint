import React, { Component } from 'react';
import './App.css';
import $ from 'jquery';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// Portfolio view
import Intro from './components/sections/intro';
import Bio from './components/sections/bio';
import Web from './components/sections/web';
import Mobile from './components/sections/mobile';
import BV from './components/sections/betterView';
import Tech from './components/sections/tech';
import Contact from './components/sections/contact';
import Menu from './components/layouts/menu';
import Footer from './components/sections/footer';
import NewSkills from './components/sections/newskills';

// discovery view
// AIzaSyAXuj-BsPlkk7h_IrqLbkxI3c2iSWdS83Q
import Map from './components/sections/map';

class App extends Component {

  componentWillMount(){
    this.fetch();
  }

  fetch(){
    $.ajax({
      method:'GET',
      url:'/api/test',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({data: data}, function(){
          console.log(this.state);
        });
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
  })}

  render() {
    return (
      <Router>
        <section>
          <Route exact path="/" component={Portfolio} />
          <Route exact path="/discovery" component={Discovery} />
        </section>
      </Router>
    );
  }
}

const Portfolio = () => (
  <div className="App">
    <Menu />
    <Intro />
    <Bio />
    <NewSkills />
    <BV />
    <Tech />
    <Web />
    <Mobile />
    <Contact />
  </div>
);

const Discovery = () => (
  <div className="Discovery">
    <h1>Discovery</h1>
    <Map />
  </div>
);


export default App;
