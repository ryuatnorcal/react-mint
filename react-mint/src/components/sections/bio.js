import React, { Component } from 'react';
import Carousel from '../carousel/carousel';
import me from '../../imgs/me3.jpg';
var bioStyle = require('../../stylesheets/bio.css');
class bio extends Component {

  constructor(){
    super();
    this.state = {};
    let data = {};
  }
  componentDidMount(){
    this.initiateState();
    this.initiateData();
  }

  initiateState(){
    this.setState({
      name: 'Ryutaro Matsuda',
      title: 'Lead Front End Developer',
      company: 'U.S. Bank'
    });
  }
  initiateData(){
    this.data = [
        {
          sec:'bio',
          category: 'intro',
          header: 'Bio',
          content: 'My name is Ryutaro Matsuda. I immigrated from Japan to the United States to pursue my dream of becoming a software engineering. I completed my general education and transfer credits at Sacramento City College. I transferred to California State University, East Bay where I completed my Bachelor of Science in computer science in 2015. Since graduating, I have developed various projects; mockup works for AEM and its integration, Hybrid Mobile Applications with ionic frameworks, Angular JS applications, and react applications. I have been fortunate in my early career to have learned my different techniques and solutions for different projects. My current career experience includes working with start-ups as well as a Fortune 500 company. I am familiar and excel working in a high-pace development environment and as part of a team.'
        },
        {
          sec:'bio',
          category: 'history',
          header: 'Lead Frontend Engineer at U.S. Bank',
          location: 'San Francisco CA',
          year: '2016 - Current',
          content: 'As Lead Front-end Engineer at U.S. Bank; I have developed front-end mock-up for AEM as well as major contributions for the U.S. Bank homepage and login widget. Additionally, I have contributed to U.S. Bank\'s internal website, product pages, and styling sheet libraries I worked closely with developer teams, designer teams, and clients to complete projects. This position has specifically expanded my knowledge of major methodologies for Adobe Experience Manager.'
        },
        {
          sec:'bio',
          category: 'history',
          header: 'Lead Frontend and Hybrid Mobile Developer at Emrl',
          location: 'Sacramento CA',
          year: '2015 - 2016',
          content: 'As Lead Hybrid Mobile / Full Stack Developer at Emrl; I developed iPhone and Android mobile apps using the ionic framework, and the client-side dashboard app with MEAN Stack. I updated Emrl’s task management system via Ruby on Rails. This assignment allowed me to develop skills in Ionic and Cordova development.'
        },
        {
          sec:'bio',
          category: 'history',
          header: 'Startup CTO and Developer at Booomindustries LLC',
          location: 'Oakland CA',
          year: '2013 - 2015',
          content: 'As CTO and Developer at Booomindustries, I have worked on a design tool for snowboard which was similar to motorola\'s phone designer with using HTML5 and JavaScripts. I was working closely with industry that builds custom desinged snowboard and other developers. However, the startup was ended before it launched to public.'
        },
        {
          sec:'bio',
          category: 'history',
          header: 'Intern Software Developer at PSP Inc',
          location: 'Bellevue WA',
          year: '2015 - 2015',
          content: 'As an Intern Software Developer at PSP Inc, I supported full-time software developers. One of my assignments was the re-development of the PSP Inc Mail Magazine product. I also developed an assignment manager for a local elementary school. This internship provided me with valuable front-end skills as well as knowledge in CakePHP.'
        }

      ];
  }

  render() {
    return (
      <section className="Bio">
      	<div className="container-fluid">
      		<div className="row">
      			<div className="col-md-6">
      				<img align="middle" className="profile" src={me} />
              <h2>{this.state.name}</h2>
              <p>{this.state.title}</p>
              <p>{this.state.company}</p>
      			</div>
            <div className="col-md-6">
              <Carousel data={this.data} target="bioCarousel"/>
            </div>
      		</div>
      	</div>
      </section>
    );
  }
}

export default bio;
