import React, { Component } from 'react';
import { compose, withProps } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");
var introStyle = require('../../stylesheets/map.css');
class map extends Component {
	constructor(){
		super();
		this.state = {
			map: {

			}
		}
	}

	setIntroProps(){
		this.setState({
			map:{
				name: 'Ryutaro Matsuda',
				title: 'Lead Front End Engineer',
				company:'U.S. Bank',
				year: '2016'
			}
		});

		console.log(this.props);
	}
	componentDidMount(){


	}


  render() {
    return (
      <section className="Map">
        <MyMapComponent id="map" />
      </section>
    );
  }

}

const MyMapComponent = compose(
    withProps({
      googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAXuj-BsPlkk7h_IrqLbkxI3c2iSWdS83Q&v=3.exp&libraries=geometry,drawing,places",
      loadingElement: <div style={{ height: `100%` }} />,
      containerElement: <div style={{ height: `80vh` }} />,
      mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
  )((props) =>
    <GoogleMap
      id="map"
      defaultZoom={8}
      defaultCenter={{ lat: 37.7749, lng: -122.4194 }}
    >
      {getMarkers()}
    </GoogleMap>
  )

  const getMarkers = () => {
    let markers = [];
      markers.push(
        <Marker
          position={{ lat: 37.7749, lng: -122.4194 }}>
          <InfoBox>
            <div>
              San Francisco
            </div>
          </InfoBox>
        </Marker>
        );
    return markers;
  }



export default map;
