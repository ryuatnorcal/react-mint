import React, { Component } from 'react';
import Icons from '../layouts/icons';
var techStyle = require('../../stylesheets/tech.css');
var fontFizz = require('../../libs/font-mfizz-2.4.1/dist/font-mfizz.css');
class tech extends Component {
	constructor(){
		super();
		let icons = [];
	}

	componentDidMount(){
		this.initiateIcons();
	}

	initiateIcons(){
		this.icons = [
			{
				name:'AngularJS',
				tag: 'icon-angular'
			},
			{
				name:'React JS',
				tag: 'icon-reactjs'
			},
			{
				name:'CSS3',
				tag: 'icon-css3'
			},
			{
				name:'HTML5',
				tag: 'icon-html5'
			},
			{
				name:'JavaScript',
				tag: 'icon-javascript'
			},
			{
				name:'NodeJS',
				tag: 'icon-nodejs'
			},
			{
				name:'JAVA',
				tag: 'icon-java'
			},
			{
				name:'PHP',
				tag: 'icon-php'
			},
			{
				name:'Responsive Design',
				tag: 'icon-mobile-device'
			},
			{
				name:'npm',
				tag: 'icon-npm'
			},
			{
				name:'React Native/Cordova',
				tag: 'icon-iphone'
			},
			{
				name:'AEM',
				tag: 'icon-shell'
				
			}

		];
	}

  render() {
  	let iconList = [];
  	if(this.icons){
  		for(var i = 0; i < this.icons.length; i++){
  			iconList.push(<Icons icon={this.icons[i]} key={i} iid={i} target="tech" />);
  		}
  	}

    return (
      <section className="Tech">
      	<div className="container-fluid">
          <div className="row">
          	<div className="col-12">
          		<div className="header">          			  		
          			<i className="fa fa-terminal" aria-hidden="true"></i>
          			<h2>Favorite Technologies</h2>
          		</div> 
          	</div>
          </div>
          <div className="row justify-content-center">
          	{iconList}
          </div>
      	</div>          
      </section>
    );
  }
}

export default tech;
