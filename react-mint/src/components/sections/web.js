import React, { Component } from 'react';
import Carousel from '../carousel/carousel';
var webStyle = require('../../stylesheets/web.css');
class web extends Component {

  constructor(){
  	super();
  	this.web = [];
  }

  initializeWeb(){
  	this.web = [
  		{
  			sec:'web',
  			title: 'U.S. Bank Homepage',
  			url: 'https://www.usbank.com',
  			desc: 'My assignment was to translate all of the visual guides into a real webpage. I collaborated closely with the design team, accessibility team, and developer team to rebrand and build the U.S. Bank homepage. I was able to manage the challenges in this project through a solution based work ethic, strong problem-solving skills, and an innovative perspective.',
  			tech:[
  				{
  					name:'AngularJS',
  					tag:'icon-angular-alt'
  				},
  				{
  					name:'Bootstrap',
  					tag:'icon-bootstrap'
  				},
  				{
  					name:'JQuery',
  					tag:'icon-jquery'
  				},
  				{
  					name:'CSS3',
  					tag:'icon-css3'
  				},
  				{
  					name:'HTML5',
  					tag:'icon-html5'
  				},
  				{
					name:'Responsive Design',
					tag: 'icon-mobile-device'
				}		
  			]
  		},
  		{
  			sec:'web',
  			title: 'Elvon FIS rebranding AEM template mockup',  			
  			desc: 'I was responsible for creating the mockups for each component of the Elavon FIS rebranding page. I closely collaborated with the client, design team, and the AEM developer team. This assignment strengthened my experience in AEM development, usage of Angular JS in AEM components, and best stylesheet practices for AEM components.',
  			tech:[
  				{
  					name:'AngularJS',
  					tag:'icon-angular-alt'
  				},
  				{
  					name:'Bootstrap',
  					tag:'icon-bootstrap'
  				},
  				{
  					name:'CSS3',
  					tag:'icon-css3'
  				},
  				{
  					name:'HTML5',
  					tag:'icon-html5'
  				},
  				{
  					name:'AEM',
  					tag:'icon-shell'
  				},
  				{
  					name:'Responsive Design',
  					tag:'icon-mobile-device'
  				}
  			]
  		},
  		{
  			sec:'web',
  			title: 'My Mobile Money Pass',  			
  			desc: 'As the main developer for this project, I developed HTML mock-ups for the AEM page and integrated them into AEM components. This opportunity increased my knowledge of the sling model, OSGI bundle, HTL, and user experience oriented design.',
  			tech:[
  				{
  					name:'AngularJS',
  					tag:'icon-angular-alt'
  				},
  				{
  					name:'Bootstrap',
  					tag:'icon-bootstrap'
  				},
  				{
  					name:'CSS3',
  					tag:'icon-css3'
  				},
  				{
  					name:'HTML5',
  					tag:'icon-html5'
  				},
  				{
  					name:'AEM',
  					tag:'icon-shell'
  				},
  				{
  					name:'Responsive Design',
  					tag:'icon-mobile-device'
  				}
  			]
  		},
  		{
  			sec:'web',
  			title: 'iPad Best Practice Angular App',  			
  			desc: 'I designed and developed U.S. Bank\'s internal website utilizing Angular JS in a short time period. The website included video content; I learned a workaround for streaming via a low bandwidth user.',
  			tech:[
  				{
  					name:'AngularJS',
  					tag:'icon-angular-alt'
  				},
  				{
  					name:'Bootstrap',
  					tag:'icon-bootstrap'
  				},
  				{
  					name:'CSS3',
  					tag:'icon-css3'
  				},
  				{
  					name:'HTML5',
  					tag:'icon-html5'
  				},
  				{
  					name:'Responsive Design',
  					tag:'icon-mobile-device'
  				}
  			]
  		},
  		{
  			sec:'web',
  			title: 'Wealth Management: Portfolio page Responsive MockUp',  			
  			desc: 'I developed the design mockup for this project in 4 hours. I successfully managed a high-pressure, fast-paced development environment, delivering the page within a very short time frame. Although the project was time sensitive; I was able to utilize bootstrap to deliver a quality product.',
  			tech:[
  				{
  					name:'AngularJS',
  					tag:'icon-angular-alt'
  				},
  				{
  					name:'Bootstrap',
  					tag:'icon-bootstrap'
  				},
  				{
  					name:'CSS3',
  					tag:'icon-css3'
  				},
  				{
  					name:'HTML5',
  					tag:'icon-html5'
  				},
  				{
  					name:'Responsive Design',
  					tag:'icon-mobile-device'
  				}
  			]
  		}
  	];
  }
  componentDidMount(){
  	this.initializeWeb();
  }
  render() {
    return (
      <section className="Web">
      	<div className="container-fluid">
         <div className="row">
         	<div className="col-12">
         		<div className="header">	         		
	         		<i className="fa fa-desktop" aria-hidden="true"></i>	
	         		<h2>WEB</h2>
	         	</div>
         	</div>
         </div>
         <div className="row"> 
         	<div className="col-12">
         		<Carousel data={this.web} target="web"/>
         	</div>
         </div>
      	</div>          
      </section>
    );
  }
}

export default web;
