import React, { Component } from 'react';
import ErrHandler from '../layouts/errHandler';
import Alert from '../layouts/alert';
import $ from 'jquery';
var contactStyle = require('../../stylesheets/contact.css');
class contact extends Component {
  

  componentWillMount(){
    this.setDefaultErr();
  }
  submitHandler(e){
    e.preventDefault();
    var refs = this.refs;
    var isReadyForSubmit = this.validateInput(refs);
    if(isReadyForSubmit){
      this.submitForm(refs);
    }          
  }
  validateInput(refs){
    var formVal = [];
    var isReadyForSubmit = true;    
    formVal.push(refs.title.value === ''? true: false);
    formVal.push(refs.name.value === ''? true: false);    
    formVal.push(refs.email.value === ''? true: false);
    formVal.push(refs.email.value.indexOf('@') < 0? true: false);
    formVal.push(refs.msg.value === ''? true: false);
    for(var i = 0; i < formVal.length; i++ ){
      if(formVal[i]){
        isReadyForSubmit = false;
        break;
      }
    }
    this.setState({
      errs: {
        msg: formVal.pop(),
        emailF:formVal.pop(),
        email: formVal.pop(),        
        name: formVal.pop(),
        title: formVal.pop()
      },
      sent:false
    });
    return isReadyForSubmit;
  }

  submitForm(ref){
    
    var packet = JSON.stringify({
      name: ref.name.value,
      email: ref.email.value,
      title: ref.title.value,
      msg: ref.msg.value
    });

    $.ajax({
      type: 'POST',
      data: packet,
      contentType: 'application/json',
      url: '/api/message',            
      success: function(data) {
        this.setState({       
          sent: true
        });
        setInterval(function(){
          this.setState({       
            sent: false
          });
        }.bind(this),5000);
      }.bind(this)
    });
  }

  setDefaultErr(){
    this.setState({
      errs:{
        title: false,
        name : false,
        email: false,  
        emailF:false,      
        msg: false  
      },
      sent: false
    });    
  }
  
  render() {
    return (      
      <section className="Contact">
      	<div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div className="header">
                <i className="fa fa-envelope-open-o" aria-hidden="true"></i>
                <h2>Contact</h2>                
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-sm-6 mailFormContainer">
              <form onSubmit={this.submitHandler.bind(this)}>
                <div className="form-group">
                  <label>Title</label>
                  {(this.state.errs.title) ? <ErrHandler field="Title" /> : null}                        
                  <input className="form-control" type="text" ref="title"/>                  
                </div>
                <div className="form-group">
                  <label>Name</label>
                  {(this.state.errs.name) ? <ErrHandler field="Name" /> : null}                  
                  <input className="form-control" type="text" ref="name"/>
                </div>
                <div className="form-group">
                  <label>Email</label>                  
                  {(this.state.errs.email) ? <ErrHandler field="Email" /> : null}
                  {(this.state.errs.emailF && !this.state.errs.email)? <ErrHandler field="@" /> : null}
                  <input className="form-control" type="email" ref="email"/>
                </div>
                <div className="form-group">
                  <label>Message</label>
                  
                  {(this.state.errs.msg) ? <ErrHandler field="Message" /> : null}
                  <textarea className="form-control" ref="msg"></textarea>
                </div>
                <div className="form-group">                  
                  <button className="form-control btn btn-submit" onClick={this.submitHandler.bind(this)}>Submit</button> 
                </div>
              </form>
            </div>
          </div>          
        </div>
        {(this.state.sent) ? <Alert field="Sent" /> : null }
      </section>
    );
  }
}

export default contact;
