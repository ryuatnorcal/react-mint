import React, { Component } from 'react';
import mobile from '../../imgs/taskmanager.png';
import desktop from '../../imgs/taskmanager-desktop.png';
var newSkillStyle = require('../../stylesheets/newSkills.css');
var fontFizz = require('../../libs/font-mfizz-2.4.1/dist/font-mfizz.css');
class NewSkills extends Component {
  constructor(){
    super();
    this.state = {}
  }

  componentDidMount = () => {
    this.setState({
      isDetail: false,
      buttonText: "SHOW DETAIL"
    })
  }

  toggleDetail = () => {
    if(this.state.isDetail){
      this.setState({
        isDetail: !this.state.isDetail,
        buttonText: "SHOW DETAIL"
      })
    }
    else {
      this.setState({
        isDetail: !this.state.isDetail,
        buttonText: "HIDE DETAIL"
      })
    }

  }

  render() {
    return (
      <section className="BulkContent">
        <section className="NewSkills">
          <div className="container">
            <div className="row mainContent">
              <div className="col-md-1 header">
                <h2>NEW SKILLS</h2>
                <p>UPDATED ON FEB 2018</p>
              </div>
              <div className="col-md-11">
                <div className="body">
                  <h2>Task Manager Web and Mobile App</h2>
                  <div className="row">
                    <div className="col-4">
                      <i className="fa fa-2x icon-reactjs" aria-hidden="true"></i>
                      <p>React JS</p>
                    </div>
                    <div className="col-4">
                      <i className="fa fa-2x icon-iphone" aria-hidden="true"></i>
                      <p>React Native</p>
                    </div>
                    <div className="col-4">
                      <i className="fa fa-2x icon-reactjs" aria-hidden="true"></i>
                      <p>Redux</p>
                    </div>
                  </div>
                  <p>
                    While studying React and React Native, I realized that importance of Redux and Ecma Script.<br />
                    The Task Manager apps were the best practice for developing full stack application with utilizing new technologies.<br />
                    I spent 2 months to develop both react web app and react native mobile app from scratch.
                  </p>
                  <p>
                    The Task Manager can create projects with start, due and ending date. You can also add tasks and comments on the project.<br />
                    If you decided to connect with the express server, mobile and web app are sharing the same projects.
                  </p>
                  <p>The demo apps are available at my bitbucket<br />
                     URL: <a href="https://bitbucket.org/ryuatnorcal/taskmanager" target="_blank">https://bitbucket.org/ryuatnorcal/taskmanager</a></p>
                </div>
              </div>
            </div>
            <div className="row footerContent">
              <div className="col-12">
                <div className="body">
                  <button className="btn btn-transparent" onClick={()=>this.toggleDetail()}>{this.state.buttonText}</button>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className={"Guide " + (this.state.isDetail? "slideDown": "slideUp")}>
          <div className="container-fluid">
            <div className="row">
              <div class="col-12 header">
                <h2>Installation Guide</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-md-4 bodyLeft hidden-sm-down">
                <img align="middle" className="screenShortDesktop" src={desktop} />
                <img align="middle" className="screenShortMobile" src={mobile} />
              </div>
              <div className="col-md-8 bodyRight">

                <p>
                  In this Installation guide, I will assume you have <a href="https://nodejs.org/en/" target="_blank">NodeJS</a>, <a href="https://www.mongodb.com/" target="_blank">MongoDB</a>, <a href="https://reactjs.org/" target="_blank">React</a> and <a href="https://facebook.github.io/react-native/" target="_blank">React Native</a> installed on your local machine.
                  If not, then please install them in order to run the task manager.
                </p>
                <p>
                  Please clone the file form bitbucket
                </p>
                <code>git clone git@bitbucket.org:ryuatnorcal/taskmanager.git</code>
                <p>
                  Once installed, you will need to run mongoDB to be running before run others.
                </p>
                <code>mongod</code>
                <p>
                  Once start mongoDB, you can run Express server at <code>taskManager-server</code>.<br />
                  At default, Express server will run on <code>port 3002</code><br />
                  Please ensure that you run <code>npm install</code> to install node dependency before running server.
                </p>
                <code>
                  cd taskManager-server<br />
                  npm install <br />
                  npm start
                </code>
                <p>
                  Next you will go in <code>task-manager-client</code> to start web application.<br />
                  At default, React server will run on <code>port 3001</code><br />
                  Please ensure that you run <code>npm install</code> to install node dependency before running server.
                </p>
                <code>
                  cd ../ <br />
                  cd task-manager-client <br />
                  npm install <br />
                  npm start
                </code>
                <p>
                  Once running both express and react server, you will see web application at <code>http://localhost:3001</code>
                </p>
                <h3>Mobile App</h3>
                <p>
                  Please note that material ui is not fully support with iOS devices, it may not correctly apearing. <br />
                  Some of dependencies requires extra step to link togather. Please follow their github documentation before the following process.
                </p>
                <h4>Dependencies</h4>
                <ul>
                  <li><a href="https://github.com/xotahal/react-native-material-ui" target="_blank">react-native-material-ui</a></li>
                  <li><a href="https://github.com/react-navigation/react-navigation" target="_blank">react-navigation</a></li>
                </ul>
                <p>
                  The mobile app is generated with non-quick generation method. Please see <a href="http://facebook.github.io/react-native/docs/getting-started.html" target="_blank">React Native get started page with Building Projects with Native Code </a>
                </p>
                <p>
                  Please install all the dependencies <br />
                  <code>
                    cd ../<br />
                    cd taskManagerMobile<br />
                    npm install<br />
                  </code>
                  Once installed the node dependency, you will be able to run the app.
                  Please ensure that xcode and/or android studio are ready to run on either emulator or device<br />
                  For iOS<br />
                  <code>react-native run-ios</code><br />
                  For Android<br />
                  <code>react-native run-android</code>
                </p>
                <p>
                  Once app is running and decided to use network, Please connect your
                  mobile device to the same WiFi as your computer is connected. <br />
                  Please find your IP4v on your computer and enter the address as prompted<br />
                  If you decided to skip the networking step, you will redirect to welcome page.
                  The non network version of app will not save your projects on local device, however,
                  while in the same session, your data will be stored in redux storage.
                </p>
                <p>
                  I hope the installation guide helps you install the web app and mobile app. <br />
                  If there any problem, please scroll all the way down to email form for contact me.
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="footer">
                  <button className="btn btn-transparent" onClick={()=>this.toggleDetail()}>{this.state.buttonText}</button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default NewSkills;
