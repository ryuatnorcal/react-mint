import React, { Component } from 'react';
var introStyle = require('../../stylesheets/intro.css');
class intro extends Component {
	constructor(){
		super();
		this.state = {
			intro: {

			}
		}
	}

	setIntroProps(){
		this.setState({
			intro:{			
				name: 'Ryutaro Matsuda',
				title: 'Lead Front End Engineer',
				company:'U.S. Bank',
				year: '2016'
			}
		});

		console.log(this.props);
	}
	componentDidMount(){
		this.setIntroProps();
	}

  render() {
    return (
      <section className="Intro">
      	<div className="container-fluid">
      		<div className="row justify-content-sm-center">      			
      			<div className="col-12 col-sm-6 col-xl-4">
      				<div className="crest">
      					<div className="inner_crest">
      						<h1>{this.state.intro.name}</h1>
      						<h4>{this.state.intro.title}</h4>
      						<p>At</p>
      						<h3>{this.state.intro.company}</h3>
      						<p>Since</p>
      						<h3>{this.state.intro.year}</h3>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>          
      </section>
    );
  }
}

export default intro;
