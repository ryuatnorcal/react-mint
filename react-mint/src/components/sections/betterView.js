import React, { Component } from 'react';
var betterViewStyle = require('../../stylesheets/betterView.css');
class betterView extends Component {

  render() {
    return (
      <section className="BetterView">
      	<div className="container-fluid">
          <div className="row justify-content-center">
          	<div className="col-sm-6">
          		<div className="text">
	          		<h2>Let's See What His Mates Say... </h2>
	          		<a href="https://betterview.com/ryutaro-matsuda" className="btn btn-bv">Visit Better View</a>
	          	</div>
          	</div>
          </div>
        </div>
      </section>
    );
  }
}

export default betterView;
