import React, { Component } from 'react';
import Carousel from '../carousel/carousel';

var mobileStyle = require('../../stylesheets/mobile.css');
var fontawesome = require('../../libs/font-awesome-4.7.0/css/font-awesome.min.css');
class mobile extends Component {
	constructor(){
		super();
		let mobiles=[];
	}

	componentDidMount(){
		this.initializeMobile();
	}
	initializeMobile(){
		this.mobiles = [
			{
				sec:'mobile',
				theme: "orange",
				name: 'Engine',
				img: 'engineApp',
				url:{
					ios: 'https://itunes.apple.com/us/app/engine-create-share-insightful/id1112265069',
					android: 'https://play.google.com/store/apps/details?id=com.mcorp.engine89450',
					web:'https://www.the-mcorp.com/engine/'
				},
				desc: 'As the project’s main developer, I learned and utilized ionic framework and hybrid mobile development methodology. I collaborated closely with clients and the design director to deliver products in a timely manner. I was also able to learn some of the limitations and possibilities associated with hybrid mobile apps. Furthermore, I developed a web-based dashboard for the admin of mobile app; this helped manage user information via MEAN stack.',
				year: '2015',
				tech:[
					{
						name:'NodeJS',
						tag: 'icon-nodejs'
					},
					{
						name:'Ionic',
						tag: 'icon-phone-gap'
					},
					{
						name:'AngularJS',
						tag: 'icon-angular-alt'
					},
					{
						name:'Bootstrap',
						tag: 'icon-bootstrap'
					},
					{
						name:'MongoDB',
						tag: 'icon-mongodb'
					}
				]				
			},
			{
				sec:'mobile',
				name: 'Snowboarding App',
				img: 'snowboardingApp',
				url:{
					ios: '',
					android: '',
					web:''
				},
				desc: 'The Snowboarding App was the final project for my Mobile Application Development course at California State University, East Bay. I utilized device location, accelerator, and Google Maps to track the user\'s location, speed, and direction. I tested the app at Squaw Valley Ski Resort in Olympic Valley, CA, during the development process. This project expanded my knowledge in native mobile app development.',
				year: '2014',
				tech:[					
					{
						name:'Native Android',
						tag: 'icon-iphone'
					},
					{
						name:'Java',
						tag: 'icon-java'
					}
				]				
			},
		];
	}


  render() {

    return (
      <section className="Mobile">
      	<div className="container-fluid">
        	<div className="row">
        		<div className="col-12">
        			<div className="header">        				
        				<i className="fa fa-mobile" aria-hidden="true"></i>
        				<h2>Mobile Apps</h2>
        			</div>
        		</div>
        		<div className="col-12">        			
        			<Carousel data={this.mobiles} target="mobile"/>
        		</div>
        	</div>
        </div>
      </section>
    );
  }
}

export default mobile;
