import React, { Component } from 'react';
import MenuHelper from './menuHelper';
var menuStyle = require('../../stylesheets/menu.css');
class menu extends Component {


  render() {
    return(
    	<section className="Menu hidden-sm-down">
    		<MenuHelper val="Intro" icon="fa-home" />
    		<MenuHelper val="Bio" icon="fa-address-card-o" />
        <MenuHelper val="NewSkills" icon="fa-plus" />
    		<MenuHelper val="Tech" icon="fa-terminal" />
    		<MenuHelper val="Web" icon="fa-desktop" />
    		<MenuHelper val="Mobile" icon="fa-mobile" />
    		<MenuHelper val="Contact" icon="fa-envelope-open-o" />
    	</section>
    );
  }
}

export default menu;
