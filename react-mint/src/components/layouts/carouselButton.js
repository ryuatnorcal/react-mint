import React, { Component } from 'react';


class carouselButton extends Component {

  render() {
    
      if(this.props.lstid == 0){
        return(<li data-target={this.props.target} data-slide-to="0" className="active"></li>);  
      }
      else{
       return(<li data-target={this.props.target} data-slide-to={this.props.lstid}></li>);   
      }
      
  }
}

export default carouselButton;
