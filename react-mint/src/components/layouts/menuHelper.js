import React, { Component } from 'react';
import $ from 'jquery';

class menuHelper extends Component {

	menuHandler(){
		var cls = "."+this.props.val;
		console.log(cls);
		$('html,body').animate({
			scrollTop:$(cls).offset().top
		},800);	
	}

  render() {    
    return(    	
        <button className="btn btn-transparent" onClick={this.menuHandler.bind(this)}><i className={'fa ' + this.props.icon } aria-hidden="true"></i></button>        
    );
  }
}

export default menuHelper;
