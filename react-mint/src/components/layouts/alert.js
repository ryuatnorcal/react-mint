import React, { Component } from 'react';

var alertStyle = require('../../stylesheets/alert.css');
class alert extends Component {


  render() {    
    
    var cat = this.props.field;
    var msg = '';
    switch(cat){
        case 'Sent':
            msg = 'Message has successfully sent! I will contact you shortly.';
            break;
    }

    return(
        <section className="Alert">
            <i className="fa fa-check fa-2x" aria-hidden="true"></i><p>{msg}</p>
        </section>
        );
    	
  
    }
}

export default alert;
