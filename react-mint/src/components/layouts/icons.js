import React, { Component } from 'react';

var iconStyle = require('../../stylesheets/icon.css');
class icons extends Component {

  render() {
      
    switch(this.props.target){
      case 'tech':
        return (
          <div className="col-6 col-sm-3 icons">
            <i className={this.props.icon.tag}></i>
            <p>{this.props.icon.name}</p>
          </div>
        );        
      case 'mobile':
      case 'web':    
        return(
          <div className="col-4 col-sm-2 icons">
            <i className={this.props.icon.tag}></i>
            <p>{this.props.icon.name}</p>
          </div>
        );
    }
  }
}

export default icons;
