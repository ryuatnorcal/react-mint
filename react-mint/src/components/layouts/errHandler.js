import React, { Component } from 'react';

var errHandlerStyle = require('../../stylesheets/errHander.css');
class errHandler extends Component {

  render() {
      return(
        <div className="errHandler">
          <p>
            {this.props.field} is required.
          </p>
        </div>
        )

  }
}

export default errHandler;
