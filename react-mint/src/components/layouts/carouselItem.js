import React, { Component } from 'react';
import Icons from './icons';
import Imgs from './imgs';
class carouselItem extends Component {
  
  render() {    
    switch(this.props.data.sec){
      case 'bio':
        if(this.props.lstid == 0){
          return(
              <div className="carousel-item active">
                <div className="container">
                  <div className="row">
                    <div className="col-12">
                      <h3>{this.props.data.header}</h3>
                    </div>
                    <div className="col-12">
                      <p>{this.props.data.content}</p>
                    </div>
                  </div>
                </div>
              </div>
            );
        }
        else{
          return(
            <div className="carousel-item">
              <div className="container">
                <div className="row">
                  <div className="col-12">
                    <h3>{this.props.data.header}</h3>
                  </div>
                  <div className="col-12">
                    <p>{this.props.data.location} {this.props.data.year}</p>
                  </div>
                  <div className="col-12">
                    <p>{this.props.data.content}</p>
                  </div>
                </div>
              </div>
            </div>
          );  
        }
        break;
      case 'web':
        let webIcons = [];
        let isUrl = this.props.data.url? '':'hide';
        if(this.props.data){                              
          for(var i = 0; i< this.props.data.tech.length; i++){            
            console.log(this.props.data.tech[i]);
            webIcons.push(<Icons target="web" icon={this.props.data.tech[i]} key={i} />);
          }          
        }
        if(this.props.lstid == 0){

          return(
            <div className="carousel-item active">
                <div className="container">
                  <div className="col-12">
                    <h3>{this.props.data.title}</h3>
                    <p>{this.props.data.desc}</p>                    
                    <div className={'linkWrapper ' + isUrl}>
                      URL: <a href={this.props.data.url}>{this.props.data.url}</a>
                    </div>
                    <div className="row justify-content-center iconContainer">
                      {webIcons}
                    </div>                    
                  </div>
                </div>
            </div>
          );
        }
        else{
          return(
            <div className="carousel-item">
                <div className="container">
                  <div className="col-12">
                    <h3>{this.props.data.title}</h3>
                    <p>{this.props.data.desc}</p>
                    <div className={'linkWrapper ' + isUrl}>
                      URL: <a href={this.props.data.url}>{this.props.data.url}</a>
                    </div>
                    <div className="row justify-content-center iconContainer">
                      {webIcons}
                    </div>                    
                  </div>
                </div>
            </div>
          );
        }
        break;
      case 'mobile':
        let mobileIcons = [];
        let isIos = this.props.data.url.ios === ''? 'hide':'';
        let isAndroid = this.props.data.url.android === ''? 'hide':'';
        let isWeb = this.props.data.url.web === ''? 'hide': '';
        let isLink = isIos == 'hide' && isAndroid == 'hide' && isWeb == 'hide'? 'hide':'';        
        if(this.props.data){                    
          for(var i = 0; i< this.props.data.tech.length; i++){            
            console.log(this.props.data.tech[i]);
            mobileIcons.push(<Icons target="mobile" icon={this.props.data.tech[i]} key={i} />);
          }          
        }
        
        if(this.props.lstid == 0){
          return(
              <div className="carousel-item active mobile" data-theme={this.props.data.theme}>
                <div className="container">
                  <div className="row">
                    <div className="col-12">
                      <h3>{this.props.data.name}</h3>     
                                  
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <Imgs icon={this.props.data.img} />
                    </div>                
                    <div className="col-md-6">
                      <p>{this.props.data.year}</p>
                      <p>{this.props.data.desc}</p>
                      <h5 className={isLink}>Links</h5>                      
                      <a className={isIos} href={this.props.data.url.ios}><i className="fa fa-2x fa-apple"></i></a>
                      <a className={isAndroid} href={this.props.data.url.android}><i className="fa fa-2x fa-android"></i></a>
                      <a className={isWeb} href={this.props.data.url.web}><i className="fa fa-2x fa-desktop"></i></a>
                      <div className="row justify-content-center tech">
                        <div className="col-12">
                          <h5>Technologeis</h5>                          
                        </div>
                        {mobileIcons}
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            );
        }
        else{
          return(
            <div className="carousel-item mobile" data-theme={this.props.data.theme}>
                <div className="container">
                  <div className="row">
                    <div className="col-12">
                      <h3>{this.props.data.name}</h3>                                            
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <Imgs icon={this.props.data.img} />
                    </div>                
                    <div className="col-md-6">
                      <p>{this.props.data.year}</p>
                      <p>{this.props.data.desc}</p>     
                      <h5 className={isLink}>Links</h5>                 
                      <a className={isIos} href={this.props.data.url.ios}><i className="fa fa-2x fa-apple"></i></a>
                      <a className={isAndroid} href={this.props.data.url.android}><i className="fa fa-2x fa-android"></i></a>
                      <a className={isWeb} href={this.props.data.url.web}><i className="fa fa-2x fa-desktop"></i></a>
                      <div className="row justify-content-center tech">
                        <div className="col-12">
                          <h5>Technologeis</h5>
                        </div>
                        {mobileIcons}
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
          );  
        }
        break;
      default:

        if(this.props.lstid == 0){
          return(
            <div className="carousel-item active">
                <h3>Under Construction</h3>                
            </div>
            );
        }
        else{
          return(
            <div className="carousel-item">
              <h3>Under Construction</h3>
            </div>
          );  
        }
      break;  
    }      
  }
}

export default carouselItem;
