import React, { Component } from 'react';
import CarouselItem from '../layouts/carouselItem';
import CarouselButton from '../layouts/carouselButton';
var carouselStyle = require('../../stylesheets/carousel.css');
class carousel extends Component {

  componentDidMount(){
    
  }

  render() {
    let carBtn = [];
    let carItm = [];
    let list = this.props.data;
    if(list){      
      for(var i = 0; i < list.length; i++ ){
        carBtn.push(<CarouselButton lstid={i} key={i} target={'#'+this.props.target}/>); 
      }
      for(var x= 0; x < list.length; x++){
        carItm.push(<CarouselItem data={list[x]} lstid={x} key={x} />);
      }
    }
    

    return (
      <div id={this.props.target} className="carousel slide" data-ride={this.props.target}>
        <ol className="carousel-indicators">
        {carBtn}          
        </ol>
        <div className="carousel-inner" role="listbox">
          {carItm}
        </div>
        <a className="carousel-control-prev" href={"#" + this.props.target} role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href={"#" + this.props.target} role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}

export default carousel;
